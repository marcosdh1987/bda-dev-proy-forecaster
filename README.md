# bda-dev-proy-infla

## Getting started

to use this repository, run the following commands:

```
git clone https://github.com/marcosdh1987/bda-dev-proy-forecaster.git
git branch -M main
git push -uf origin main
```

## make your own branch to work on

git checkout -b my-branch

# switching back to master branch

git checkout master


## Integrate with your tools



## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy


## Description
In this overview notebook, we will address business problems regarding "Inflation"


## Objective:

Financial prediction problems are of great practical and theoretical interest. They are also quite
daunting. Theory suggests that much information relevant to  nancial prediction problems may
be spread throughout available economic and other data, an idea that also gains support from
the many disparate data sources that di erent market participants watch for clues on future price
movements.

## Background:

Whitepaper references:

- [Livia Paranhos - University of Warwick - Predicting Inflation with Neural Networks - Nov 2020](docs/Predicting_Inflation_with_Neural_Networks.pdf)
- [S Zahara, Sugianto - Consumer price index prediction using Long Short Term Memory (LSTM) based cloud computing - 2020](docs/LSTM_Infla_AWS.pdf)
- [J.B.Heaton, N.G.Polson, J.H.Witte - Deep Learning in Finance SP500 index - Feb 2016](docs/ML_Financial_SP500_LSTM.pdf)


## Roadmap

- [ ] [ETL](ntbk/00-infla-etl.ipynb)
- [ ] [Exploratoy Data Analysis](ntbk/00-infla-eda.ipynb)
- [ ] [Data Modeling - Infla SKlearn Forecaster](ntbk/01-Infla_forecaster_sklearn.ipynb)
- [ ] [Data Modeling - BTC SKlearn Forecaster](ntbk/01-BTC_forecaster_sklearn.ipynb)
- [ ] [Machine Learning - LSTM]()

## Prerequisites
- JupyterLab or Sagemaker Studio.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status

Some visualizations 

![image info](./img/var_mensual.png)

Prediction of inflation using RRN vs Moving Average

![image info](./img/infla_prediction_01.png)
